# Copyright (C) 2023 European Spallation Source ERIC

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile


APP:=ADCSimDetectorApp
APPDB:=$(APP)/Db
APPSRC:=$(APP)/src

USR_INCLUDES += -I$(where_am_I)$(APPSRC)

TEMPLATES += $(APPDB)/ADCSimDetector.template
TEMPLATES += $(APPDB)/ADCSimDetector_settings.req
TEMPLATES += $(APPDB)/ADCSimDetectorN.template
TEMPLATES += $(APPDB)/ADCSimDetectorN_settings.req

HEADERS   += $(APPSRC)/ADCSimDetector.h
SOURCES   += $(APPSRC)/ADCSimDetector.cpp
DBDS      += $(APPSRC)/ADCSimDetectorSupport.dbd

SCRIPTS += $(wildcard ../iocsh/*.iocsh)

.PHONY: vlibs
vlibs:

